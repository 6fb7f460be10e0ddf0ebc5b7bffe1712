#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>

#define k16 (1<<14)
int main(int argc, char *argv[]) {
  int fd = 0;
  if (argc > 1 && (fd = open(argv[1], O_RDONLY)) == -1) return 1;
  struct stat st;
  fstat(fd, &st);
  char outbuf[k16], xdigits[] = "000102030405060708090a0b0c0d0e0f"
                                "101112131415161718191a1b1c1d1e1f"
                                "202122232425262728292a2b2c2d2e2f"
                                "303132333435363738393a3b3c3d3e3f"
                                "404142434445464748494a4b4c4d4e4f"
                                "505152535455565758595a5b5c5d5e5f"
                                "606162636465666768696a6b6c6d6e6f"
                                "707172737475767778797a7b7c7d7e7f"
                                "808182838485868788898a8b8c8d8e8f"
                                "909192939495969798999a9b9c9d9e9f"
                                "a0a1a2a3a4a5a6a7a8a9aaabacadaeaf"
                                "b0b1b2b3b4b5b6b7b8b9babbbcbdbebf"
                                "c0c1c2c3c4c5c6c7c8c9cacbcccdcecf"
                                "d0d1d2d3d4d5d6d7d8d9dadbdcdddedf"
                                "e0e1e2e3e4e5e6e7e8e9eaebecedeeef"
                                "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff";
  size_t i;
  for (i = 0; i < k16; i+=8) memcpy(outbuf+i, "'\\xXX', ", 8);
  i = 0;

  unsigned char buf[k16], *map;
  if (st.st_size < k16 ||
      (map = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
    ssize_t r;
    while ((r = read(fd, (char *)buf, k16)) > 0) {
      for (size_t j = 0; j < (size_t)r; j++) {
        outbuf[i+3] = xdigits[    2 * buf[j]];
        outbuf[i+4] = xdigits[1 + 2 * buf[j]];
        if ((i+=8) == k16) i = 0, write(1, outbuf, k16);
      }
    }
  }
  else {
    madvise(map, st.st_size, MADV_SEQUENTIAL|MADV_WILLNEED);
    for (unsigned char *end = map + st.st_size; map < end; map++) {
      outbuf[i+3] = xdigits[    2 * *map];
      outbuf[i+4] = xdigits[1 + 2 * *map];
      if ((i+=8) == k16) i = 0, write(1, outbuf, k16);
    }
  }
  if (i) write(1, outbuf, i);
  return 0;
}
